#!/bin/bash

awk '{ 
average = ($2 + $3 + $4) / 3
if (average > 80) {
    print $1,$2,$3,$4,": A"
} 
if (average > 60 && average < 80){
    print $1,$2,$3,$4,": B"
}
if (average > 50 && average < 60){
    print $1,$2,$3,$4,": C"
}
if (average < 50){
    print $1,$2,$3,$4,": FAIL"
}
}' 