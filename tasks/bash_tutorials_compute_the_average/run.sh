#!/bin/bash


read N

COUNTER=0
SUM=0

while [  $COUNTER -lt $N ]; do
    read x
    let COUNTER=COUNTER+1 
    let SUM=SUM+$x
done

printf "%.3f\n" $(echo "scale=4;$SUM / $N" | bc)