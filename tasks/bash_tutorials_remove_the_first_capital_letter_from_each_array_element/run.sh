#!/bin/bash

while read x
do 
    echo ".${x:1}" 
done | awk '{print}' ORS=' ' | (read x; echo "$x")
