#!/bin/bash

COUNTER=0

while read STR
do
if [[ "$COUNTER" -gt "2" && "$COUNTER" -lt "8" ]];
then
  echo "$STR"
fi
let COUNTER=COUNTER+1
done  | awk '{print}' ORS=' ' | (read x; echo "$x")
