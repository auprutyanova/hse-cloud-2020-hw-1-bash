#!/bin/bash

uniq -c | (while read line;
do
    echo "$(echo $line)"
done
) | head -c -2