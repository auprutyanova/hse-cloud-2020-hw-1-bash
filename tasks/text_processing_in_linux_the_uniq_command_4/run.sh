#!/bin/bash

uniq -u | (while read line;
do
    echo "$(echo $line)"
done
) | head -c -2